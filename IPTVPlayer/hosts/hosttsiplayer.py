# -*- coding: utf-8 -*-
###################################################
# LOCAL import
###################################################
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import TranslateTXT as _, GetIPTVNotify
from Plugins.Extensions.IPTVPlayer.components.ihost import CHostBase, CBaseHostClass, CDisplayListItem
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc, GetTmpDir, GetPluginDir
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


###################################################
# FOREIGN import
###################################################
import re
import base64
from Tools.Directories import resolveFilename, SCOPE_PLUGINS
from Components.config import config, ConfigText, getConfigListEntry
from os import remove as os_remove, path as os_path, system as os_system
###################################################

config.plugins.iptvplayer.ts_xtream_user = ConfigText(default = '', fixed_size = False)
config.plugins.iptvplayer.ts_xtream_pass = ConfigText(default = '', fixed_size = False)
config.plugins.iptvplayer.ts_xtream_host = ConfigText(default = '', fixed_size = False)
config.plugins.iptvplayer.ts_xtream_ua = ConfigText(default = '', fixed_size = False)

def GetConfigList():
	optionList = []
	optionList.append( getConfigListEntry(_("Xtream User:"), config.plugins.iptvplayer.ts_xtream_user) )
	optionList.append( getConfigListEntry(_("Xtream Pass:"), config.plugins.iptvplayer.ts_xtream_pass) )
	optionList.append( getConfigListEntry(_("Xtream Host:"), config.plugins.iptvplayer.ts_xtream_host) )	
	optionList.append( getConfigListEntry(_("Xtream User Agent:"), config.plugins.iptvplayer.ts_xtream_ua) )	
	return optionList



def gettytul():
	return 'TS IPlayer'

class TVProart(CBaseHostClass):

	tsiplayerversion = "2019.03.08.0" 
	tsiplayerremote  = "0.0.0.0"
	
	def __init__(self):
		CBaseHostClass.__init__(self, {'history':'MyVodanimekom', 'cookie':'myvodanimekom.cookie'})
		self.DEFAULT_ICON_URL = 'https://i.ibb.co/Q8ZRP0X/yaq9y3ab.png'
		
# Menu
		self.FilmsSeries_TAB = [
							{'category':'xtream_vod','title': 'Xtream IPTV (VOD)','icon':'https://i.ibb.co/nPHsSDp/xtream-code-iptv.jpg'},	
							{'category':'animekom','title': 'Animekom.Com','icon':'https://www.animekom.com/templates/vfilmze/images/logo.png'},
							{'category':'movs4u','title': 'Movs4u.Tv','icon':'https://www.movs4u.tv/wp-content/uploads/2019/01/Logo-header.png'},
							{'category':'cima4u','title': 'Cima4u.TV','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png'},	
							{'category':'cimaclub','title': 'CimaClub.Com','icon':'https://i.pinimg.com/originals/f2/67/05/f267052cb0ba96d70dd21e41a20a522e.jpg'},	
							#{'category':'33sk','title': '33sk.Tv','icon':'https://33sk.tv/images/logo-footer.png'}, 	
							{'category':'arablionz','title': 'Arblionz.Com','icon':'https://i.ibb.co/16pJgMF/unnamed.jpg'},								
							]	


		self.Sports_TAB = [		
		
							{'category':'xtream_live','title': 'Xtream IPTV (LIVE)','icon':'https://i.ibb.co/nPHsSDp/xtream-code-iptv.jpg'},		
							{'category':'sporttv','title': 'Sport Tv V2.0.6 (Android App)','gnr':'sporttv','icon':'https://i.ibb.co/cJ3jY40/8p4hc4g9.png'},	
							{'category':'sporttv','title': 'WSS V2.3 (Android App)','gnr':'wss','icon':'https://i.ibb.co/xYDVLNY/cdordh6s.jpg'},	
							{'category':'yallashoot','title': 'Yalla-Shoot.Com (Only replay)','icon':'http://www.yalla-shoot.com/img/logo.png','page':1},							
							]	

							
# 33sk							
		self._33sk_TAB = [
							{'category':'33sk_itm','gnr':'film','page':1,'title': 'الأفلام','icon':'https://33sk.tv/images/logo-footer.png','url':'https://33sk.tv/pdep42-'},
							{'category':'33sk_itm','gnr':'serie','page':1,'title': 'جميع المسلسلات','icon':'https://33sk.tv/images/logo-footer.png','url':'https://33sk.tv/vb/'},
							{'category':'33sk_itm','gnr':'last','page':1,'title': 'اخر الحلقات','icon':'https://33sk.tv/images/logo-footer.png','url':'https://33sk.tv/'},
							]


							
# Cimaclub							
		self.Cimaclub_TAB = [
							{'category':'Cimaclub_cat','gnr':'film','title': 'Films','icon':'https://i.pinimg.com/originals/f2/67/05/f267052cb0ba96d70dd21e41a20a522e.jpg'},
							{'category':'Cimaclub_cat','gnr':'serie','title': 'Series','icon':'https://i.pinimg.com/originals/f2/67/05/f267052cb0ba96d70dd21e41a20a522e.jpg'},
							{'category':'Cimaclub_cat','gnr':'other','page':1,'title': 'Other','icon':'https://i.pinimg.com/originals/f2/67/05/f267052cb0ba96d70dd21e41a20a522e.jpg'},
							{'category':'search','title': _('Search'), 'search_item':True,'page':1,'hst':'cimaclub','icon':'https://i.pinimg.com/originals/f2/67/05/f267052cb0ba96d70dd21e41a20a522e.jpg'},
							]


# Arablionz							
		self.Arablionz_TAB = [
							{'category':'Arablionz_cat','gnr':'film','title': 'Films','icon':'https://i.ibb.co/16pJgMF/unnamed.jpg'},
							{'category':'Arablionz_cat','gnr':'serie','title': 'Series','icon':'https://i.ibb.co/16pJgMF/unnamed.jpg'},
							{'category':'Arablionz_cat','gnr':'music','title': 'اغاني وكليبات','icon':'https://i.ibb.co/16pJgMF/unnamed.jpg'},							
							{'category':'arablionz_itm','gnr':'other','page':1,'url':'https://arblionz.com/category/%D8%A8%D8%B1%D8%A7%D9%85%D8%AC-%D8%AA%D9%84%D9%8A%D9%81%D8%B2%D9%8A%D9%88%D9%86%D9%8A%D8%A9/','title': 'برامج تليفزيونية','icon':'https://i.ibb.co/16pJgMF/unnamed.jpg'},
							{'category':'arablionz_itm','gnr':'other','page':1,'url':'https://arblionz.com/category/%D8%B9%D8%B1%D9%88%D8%B6-%D8%A7%D9%84%D9%85%D8%B5%D8%A7%D8%B1%D8%B9%D8%A9/','title': 'رياضة و مصارعه','icon':'https://i.ibb.co/16pJgMF/unnamed.jpg'},
							{'category':'search','title': _('Search'), 'search_item':True,'page':1,'hst':'arablionz','icon':'https://i.ibb.co/16pJgMF/unnamed.jpg'},
							]

							
# Cima4u							
		self.Cima4u_TAB = [
							{'category':'Cima4u_films_t','title': 'Films','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png'},
							{'category':'Cima4u_series_t','title': 'Series','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png'},
							{'category':'Cima4uFS','page':1,'title': 'WWE','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://cima4u.tv/category/%d9%85%d8%b5%d8%a7%d8%b1%d8%b9%d8%a9-%d8%ad%d8%b1%d8%a9-wwe/'},
							{'category':'Cima4uSe','page':1,'title': 'برامج تلفزيونية','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/24.%D8%A8%D8%B1%D8%A7%D9%85%D8%AC+%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86%D9%8A%D8%A9.html'},							
							{'category':'search','title': _('Search'), 'search_item':True,'page':1,'hst':'cima4u','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png'},
							]
							
		self.Cima4u_films_Cat = [{'category':'Cima4uFS','page':1,'title': 'افلام عربي','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://cima4u.tv/category/%d8%a7%d9%81%d9%84%d8%a7%d9%85-%d8%b9%d8%b1%d8%a8%d9%8a-arabic-movies/'},
								{'category':'Cima4uFS','page':1,'title': 'افلام اجنبي','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://cima4u.tv/category/%d8%a7%d9%81%d9%84%d8%a7%d9%85-%d8%a7%d8%ac%d9%86%d8%a8%d9%8a-movies-english/'},
								{'category':'Cima4uFS','page':1,'title': 'افلام كرتون','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://cima4u.tv/category/%d8%a7%d9%81%d9%84%d8%a7%d9%85-%d9%83%d8%b1%d8%aa%d9%88%d9%86-movies-anime-cartoon/'},
								{'category':'Cima4uFS','page':1,'title': 'افلام هندي','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://cima4u.tv/category/%d8%a7%d9%81%d9%84%d8%a7%d9%85-%d9%87%d9%86%d8%af%d9%8a-indian-movies/'},
								]

		self.Cima4u_series_Cat = [{'category':'Cima4uSe','page':1,'title': 'مسلسلات وبرامج رمضان 2018','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/34.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D9%88%D8%A8%D8%B1%D8%A7%D9%85%D8%AC+%D8%B1%D9%85%D8%B6%D8%A7%D9%86+2018.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات وبرامج رمضان 2017','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/29.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D9%88%D8%A8%D8%B1%D8%A7%D9%85%D8%AC+%D8%B1%D9%85%D8%B6%D8%A7%D9%86+2017.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات رمضان 2016','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/28.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D8%B1%D9%85%D8%B6%D8%A7%D9%86+2016.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات اجنبى','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/25.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D8%A7%D8%AC%D9%86%D8%A8%D9%89.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات مصريه','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/10.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D9%85%D8%B5%D8%B1%D9%8A%D9%87.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات خليجيه','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/11.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D8%AE%D9%84%D9%8A%D8%AC%D9%8A%D9%87.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات مدبلجة','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/33.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D9%85%D8%AF%D8%A8%D9%84%D8%AC%D8%A9.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات تركيه مترجمه','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/13.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D8%AA%D8%B1%D9%83%D9%8A%D9%87+%D9%85%D8%AA%D8%B1%D8%AC%D9%85%D9%87.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات تركيه مدبلجه','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/14.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D8%AA%D8%B1%D9%83%D9%8A%D9%87+%D9%85%D8%AF%D8%A8%D9%84%D8%AC%D9%87.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات اسيوية مترجمة','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/19.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D8%A7%D8%B3%D9%8A%D9%88%D9%8A%D8%A9+%D9%85%D8%AA%D8%B1%D8%AC%D9%85%D8%A9.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات اسيوية مدبلجة','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/20.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D8%A7%D8%B3%D9%8A%D9%88%D9%8A%D8%A9+%D9%85%D8%AF%D8%A8%D9%84%D8%AC%D8%A9.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات هندية مترجمة','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/23.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D9%87%D9%86%D8%AF%D9%8A%D8%A9+%D9%85%D8%AA%D8%B1%D8%AC%D9%85%D8%A9.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات هندية مدبلجة','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/22.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D9%87%D9%86%D8%AF%D9%8A%D8%A9+%D9%85%D8%AF%D8%A8%D9%84%D8%AC%D8%A9.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات انيمي مترجمة','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/17.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D8%A7%D9%86%D9%8A%D9%85%D9%8A+%D9%85%D8%AA%D8%B1%D8%AC%D9%85%D8%A9.html'},
						   {'category':'Cima4uSe','page':1,'title': 'مسلسلات انيمي مدبلجة','icon':'https://apkplz.net/storage/images/aflam/egybest/film/aflam.egybest.film_1.png','link':'http://live.cima4u.tv/16.%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA+%D8%A7%D9%86%D9%8A%D9%85%D9%8A+%D9%85%D8%AF%D8%A8%D9%84%D8%AC%D8%A9.html'},
							]							
# Animekom							
		self.Animekom_TAB = [
							{'category':'lstcata','title': 'Animes','icon':'https://www.animekom.com/templates/vfilmze/images/logo.png'},
							{'category':'lstcatf','title': 'Films','icon':'https://www.animekom.com/templates/vfilmze/images/logo.png'},							
							{'category':'search','title': _('Search'), 'search_item':True,'page':1,'hst':'animkom','icon':'https://www.animekom.com/templates/vfilmze/images/logo.png'},
							]
					
		self.Anim_CAT_TAB = [{'category':'lstitm','title': 'Animes','Url':'http://www.animekom.com/animes/','gnr':'Anim','page':1,'icon':'https://www.animekom.com/templates/vfilmze/images/logo.png','gnr':'anim'},
							{'category':'lsttyp','title': 'Animes Par Genre','gnr':'Anim','icon':'https://www.animekom.com/templates/vfilmze/images/logo.png'},
							]	

		self.Film_CAT_TAB = [{'category':'lstitm','title': 'Films','Url':'http://www.animekom.com/movies/','gnr':'Film','page':1,'icon':'https://www.animekom.com/templates/vfilmze/images/logo.png','gnr':'anim'},
							{'category':'lsttyp','title': 'Films Par Genre','gnr':'Film','icon':'https://www.animekom.com/templates/vfilmze/images/logo.png'},
							]

# Movs4u
		self.Movs4u_TAB =  [{'category':'cat_films_movs4u' ,       'title': 'Films',                  'url':'https://www.movs4u.tv/movie/'      ,'icon':'https://www.movs4u.tv/wp-content/uploads/2019/01/Logo-header.png'},
							{'category':'list_items_movs4u',       'title': 'Series','gnr':'serie'  ,'Url':'https://www.movs4u.tv/tvshows/'    ,'icon':'https://www.movs4u.tv/wp-content/uploads/2019/01/Logo-header.png','page':1},
							{'category':'search',           'title': _('Search'), 'search_item':True,'page':1,'hst':'movs4u'       ,'icon':'https://www.movs4u.tv/wp-content/uploads/2019/01/Logo-header.png'},
							]
							
		self.Movs4u_Film_CAT_TAB = [{'category':'list_items_movs4u','title': 'Films','Url':'https://www.movs4u.tv/movie/','gnr':'film','page':1,'icon':'https://www.movs4u.tv/wp-content/uploads/2019/01/Logo-header.png'},
							{'category':'list_items_movs4u','title': 'Collection Films','Url':'https://www.movs4u.tv/collection/','gnr':'Film_coll','icon':'https://www.movs4u.tv/wp-content/uploads/2019/01/Logo-header.png','page':1},
							{'category':'movs4u_lsttyp','title': 'Films Par Genre','gnr':'film','icon':'https://www.movs4u.tv/wp-content/uploads/2019/01/Logo-header.png'},
							{'category':'movs4u_lsttyp','title': 'Films Par Qualité','gnr':'Film_qual','icon':'https://www.movs4u.tv/wp-content/uploads/2019/01/Logo-header.png'},
							]
					
        
							
###################################################
# MAIN CATEGORY
###################################################	
	def MainCat(self):
		params = {'category' : 'FilmsSeries','title':'Films, Series & Animes','desc':'شاهد الافلام و الانمي والمسلسلات العربيه والاجنبيه المترجمه و المدبلجه','icon':'https://i.ibb.co/Fgk8Yq4/tsiplayer-films.png'} 
		self.addDir(params)	
		params = {'category' : 'Sports','title':'Live Tv & Replay','desc':'Live Tv & Replay','icon':'https://i.ibb.co/Fgk8Yq4/tsiplayer-films.png'} 
		self.addDir(params)
		
		self.GetVersions()
		if (self.tsiplayerversion == self.tsiplayerremote):
			color='\c00??????'
			titre_='INFO'
			img_='https://i.ibb.co/Q8ZRP0X/yaq9y3ab.png'
		else:
			color='\c0000????'
			titre_=' ---> UPDATE <--- '
			img_='https://i.ibb.co/fVR0HL6/tsiplayer-update.png'
		params = {'category' : 'update','title':color+titre_,'desc':'تحديث البرنامج','icon':img_} 
		self.addDir(params)	
		
###################################################
# UPDATE
###################################################	
	def GetVersions(self):
		printDBG( 'GetVersions begin' )
		_url = 'https://gitlab.com/Rgysoft/iptv-host-e2iplayer/raw/master/IPTVPlayer/hosts/hosttsiplayer.py'	
		query_data = { 'url': _url, 'use_host': False, 'use_cookie': False, 'use_post': False, 'return_data': True }
		try:
			data = self.cm.getURLRequestData(query_data)
			printDBG( 'Host init data: '+data )
			r=self.cm.ph.getSearchGroups(data, '''tsiplayerversion = ['"]([^"^']+?)['"]''', 1, True)[0]
			if r:
				printDBG( 'tsiplayerremote = '+r )
				self.tsiplayerremote=r
		except:
			printDBG( 'Host init query error' )
				

		
	def GetCommits(self):
		printDBG( 'GetCommits begin' )
		try:
			data = self.cm.getURLRequestData({ 'url': 'https://gitlab.com/Rgysoft/iptv-host-e2iplayer/commits/master.atom', 'use_host': False, 'use_cookie': False, 'use_post': False, 'return_data': True })
		except:
			printDBG( 'Host listsItems query error' )
		printDBG( 'Host listsItems data: '+data )
		phCats = re.findall("<entry>.*?<title>(.*?)</title>.*?<updated>(.*?)</updated>.*?<name>(.*?)</name>", data, re.S)
		if phCats:
			for (phTitle, phUpdated, phName ) in phCats:
				phUpdated = phUpdated.replace('T', '   ')
				phUpdated = phUpdated.replace('Z', '   ')
				phUpdated = phUpdated.replace('+01:00', '   ')
				phUpdated = phUpdated.replace('+02:00', '   ')
				printDBG( 'Host listsItems phTitle: '+phTitle )
				printDBG( 'Host listsItems phUpdated: '+phUpdated )
				printDBG( 'Host listsItems phName: '+phName )
				params = {'category' : 'none','title':phUpdated+' '+phName+'  >>  '+phTitle,'desc':phUpdated+' '+phName+'  >>  '+phTitle,'name':'update'} 
				self.addDir(params)	
		
	def Update(self):
		printDBG( 'Update begin' )
		params = {'category' : 'none','title':'Local version: '+str(self.tsiplayerversion)+'  |  '+'Remote version: '+str(self.tsiplayerremote),'name':'update'} 
		self.addDir(params)	

		params = {'category' : 'log','title':'ChangeLog','name':'update'} 
		self.addDir(params)	
		if (self.tsiplayerversion != self.tsiplayerremote):
			params = {'category' : 'update_now','title':'\c0000????'+' ++++++++++++ UPDATE NOW ++++++++++++ ','name':'update'} 
			self.addDir(params)	
			params = {'category' : 'update_now','title':'\c0000????'+' +++++++ UPDATE NOW & RESTART +++++++ ','name':'update_restart'} 
			self.addDir(params)	
		params = {'category' : 'thx','title':'Thanks','name':'thx'} 
		self.addDir(params)				
		printDBG( 'Host getInitList end' )
	def thx(self):
		self.addMarker({'title':'Special thank to \c0000??00 samsamsam \c00?????? the Main developer & all Developer Team','desc':''})	
		self.addMarker({'title':'Special thank to \c0000???? mamrot \c00?????? & \c0000???? mosz_nowy \c00?????? (update script)','desc':''})	

	def update_now(self,cItem):
		name_type=cItem['name']
		printDBG( 'update_now begin' )
		_url = 'https://gitlab.com/Rgysoft/iptv-host-e2iplayer'
		query_data = { 'url': _url, 'use_host': False, 'use_cookie': False, 'use_post': False, 'return_data': True }
		try:
			data = self.cm.getURLRequestData(query_data)
			r=self.cm.ph.getSearchGroups(data, '''/Rgysoft/iptv-host-e2iplayer/commit/([^"^']+?)['"]''', 1, True)[0]
			if r:
				printDBG( 'crc = '+r )
				crc=r
		except:
			printDBG( 'Host init query error' )

		tmpDir = GetTmpDir() 
		source = os_path.join(tmpDir, 'iptv-host-e2iplayer.tar.gz') 
		dest = os_path.join(tmpDir , '') 
		_url = 'https://gitlab.com/Rgysoft/iptv-host-e2iplayer/repository/archive.tar.gz?ref=master'              
		output = open(source,'wb')
		query_data = { 'url': _url, 'use_host': False, 'use_cookie': False, 'use_post': False, 'return_data': True }
		try:
			output.write(self.cm.getURLRequestData(query_data))
			output.close()
			os_system ('sync')
			printDBG( 'sync iptv-host-e2iplayer.tar.gz' )
		except:
			if os_path.exists(source):
				os_remove(source)
			printDBG( 'Download Error iptv-host-e2iplayer.tar.gz' )
			return ''			  
		if os_path.exists(source):
			printDBG( 'exists '+source )
		else:
			printDBG( 'not exists '+source )

		cmd = 'tar -xzf "%s" -C "%s" 2>&1' % ( source, dest )  
		try: 
			os_system (cmd)
			os_system ('sync')
			printDBG( 'sync ' + cmd )
		except:
			printDBG( 'Unpacking error iptv-host-e2iplayer.tar.gz' )
			os_system ('rm -f %s' % source)
			os_system ('rm -rf %siptv-host-e2iplayer-%s' % (dest, crc))
			return ''


		try:
			od = '%siptv-host-e2iplayer-master-%s/'% (dest, crc)
			do = resolveFilename(SCOPE_PLUGINS, 'Extensions/') 
			cmd = 'cp -rf "%s"/* "%s"/ 2>&1' % (os_path.join(od, 'IPTVPlayer'), os_path.join(do, 'IPTVPlayer'))
			printDBG('duplication cmd[%s]' % cmd)
			os_system (cmd)
			os_system ('sync')
		except:
			printDBG( 'copy error' )
			os_system ('rm -f %s' % source)
			os_system ('rm -rf %siptv-host-e2iplayer-master-%s' % (dest, crc))
			return ''


		ikony = GetPluginDir('icons/PlayerSelector/')
		if os_path.exists('%stsiplayer100' % ikony):
			printDBG( 'Hosttsiplayer Jest '+ ikony + 'tsiplayer100 ' )
			os_system('mv %stsiplayer100 %stsiplayer100.png' % (ikony, ikony)) 
		if os_path.exists('%stsiplayer120' % ikony):
			printDBG( 'icon Jest '+ ikony + 'tsiplayer120 '  )
			os_system('mv %stsiplayer120 %stsiplayer120.png' % (ikony, ikony))
		if os_path.exists('%stsiplayer135' % ikony):
			printDBG( 'Hosttsiplayer icon '+ ikony + 'tsiplayer135 '  )
			os_system('mv %stsiplayer135 %stsiplayer135.png' % (ikony, ikony))

		try:
			cmd = GetPluginDir('hosts/hosttsiplayer.py')
			with open(cmd, 'r') as f:  
				data = f.read()
				f.close() 
				wersja = re.search('tsiplayerversion = "(.*?)"', data, re.S)
				aktualna = wersja.group(1)
				printDBG( 'current version '+aktualna )
		except:
			printDBG( 'Hosttsiplayer error openfile ' )


		printDBG( 'Hosttsiplayer deleting temporary files' )
		os_system ('rm -f %s' % source)
		os_system ('rm -rf %siptv-host-e2iplayer-master-%s' % (dest, crc))

		 
		if (name_type == 'update_restart'):
			try:
				msg = 'Restart GUI .\n \n new version= %s' % aktualna
				GetIPTVNotify().push('%s' % msg, 'info', 20)
				GetIPTVNotify().push('%s' % msg, 'info', 2)				
				from enigma import quitMainloop
				quitMainloop(3)
			except Exception as e:
				printDBG( 'Erreur='+str(e) )				
				pass			
		 
		params = {'category' : 'none','title':'Update End. Please manual restart enigma2','name':'update'} 
		self.addDir(params)				  
		return ''

		
###################################################
# HOST Xstream live and vod
###################################################			
	def xtreamvodcat(self):
		xuser = config.plugins.iptvplayer.ts_xtream_user.value
		xpass = config.plugins.iptvplayer.ts_xtream_pass.value	
		xhost = config.plugins.iptvplayer.ts_xtream_host.value	
		xua = config.plugins.iptvplayer.ts_xtream_ua.value
		if ((xuser!='') and (xpass!='') and (xhost!='')):
			params = {'title':'\c0000??00Films','icon':'','desc':''}
			self.addMarker(params)		
		
			Url='http://'+xhost+'/enigma2.php?username='+xuser+'&password='+xpass+'&type=get_vod_categories'
			sts, data = self.cm.getPage(Url)
			Live_Cat_data = re.findall('<title>(.*?)</title>.*?<description>(.*?)</description>.*?CDATA\[(.*?)]',data, re.S)
			for (titre,desc,Url) in Live_Cat_data:
				name = base64.b64decode(titre)
				name = re.sub('\|.*?\|', '', name)
				desc = base64.b64decode(desc)
				params = {'name':'categories','category' : 'xtream_itm_vod','url': Url,'title':name,'desc':desc,'ua':xua} 
				self.addDir(params)
			try:	
				params = {'title':'\c0000??00Series','icon':'','desc':''}
				self.addMarker(params)
				
				Url='http://'+xhost+'/player_api.php'
				lst =[]
				post_data = {'username':xuser, 'password':xpass, 'action':'get_series'}

				sts, data = self.cm.getPage(Url, post_data=post_data)
				Live_Cat_data = re.findall('"name":"(.*?)".*?series_id":(.*?),.*?cover":"(.*?)".*?genre":"(.*?)".*?rating":"(.*?)"',data, re.S)
				for (titre,id_,img_,genre,rating) in Live_Cat_data:
					desc = 'GENRE:'+genre+' RATING:'+rating+'/10'
					img_ = str(img_).replace('\\','')
					params = {'name':'categories','category' : 'vodserieid','url': id_,'title':titre,'desc':desc,'icon':img_} 
					self.addDir(params)					
			except:
				pass
		else:
			params = {'title':'Please configure xstream first','icon':'','desc':'Please configure xstream first, (add user,pass &host in tsiplayer params)'}
			self.addMarker(params)	 

	def VodSerieSaisons(self,cItem):
		xuser = config.plugins.iptvplayer.ts_xtream_user.value
		xpass = config.plugins.iptvplayer.ts_xtream_pass.value	
		xhost = config.plugins.iptvplayer.ts_xtream_host.value	
		xua = config.plugins.iptvplayer.ts_xtream_ua.value	
		Url='http://'+xhost+'/player_api.php'
		id_=cItem['url']
		img_=cItem['icon']
		liste = []
		post_data = {'username':xuser, 'password':xpass, 'action':'get_series_info','series_id':id_}
		sts, data = self.cm.getPage(Url, post_data=post_data)
		Liste_films_data2 = re.findall('"episodes":\{(.*)', data, re.S)
		if Liste_films_data2:
			data2=Liste_films_data2[0]
		else:
			data2=''
		Live_Cat_data = re.findall('id":"(.*?)"episode_num":(.*?)"title":"(.*?)".*?season":(.*?),',data2, re.S)
		for (id2_,x1,titre,season) in Live_Cat_data:
			elm = ('Season '+season,id_+'|'+season)
			if ( elm not in liste ):
				liste.append(elm)	
				params = {'good_for_fav':True,'name':'categories','category' : 'vodsaisonid','url': season,'title':'Season '+season,'icon':img_,'id_':id_} 
				self.addDir(params)	

	def VodSerieEpisodes(self,cItem):
		xuser = config.plugins.iptvplayer.ts_xtream_user.value
		xpass = config.plugins.iptvplayer.ts_xtream_pass.value	
		xhost = config.plugins.iptvplayer.ts_xtream_host.value	
		xua = config.plugins.iptvplayer.ts_xtream_ua.value	
		Url='http://'+xhost+'/player_api.php'
		id_=cItem['id_']
		saison=cItem['url']		
		img_=cItem['icon']
		post_data = {'username':xuser, 'password':xpass, 'action':'get_series_info','series_id':id_}
		sts, data = self.cm.getPage(Url, post_data=post_data)
		Liste_films_data2 = re.findall('"episodes":\{(.*)', data, re.S)
		if Liste_films_data2:
			data2=Liste_films_data2[0]
		else: 
			data2=''
		Live_Cat_data = re.findall('id":"(.*?)","episode_num":(.*?)"title":"(.*?)","container_extension":"(.*?)".*?season":(.*?),',data2, re.S)
		for (id_,x1,titre,ext,season) in Live_Cat_data:
			link = 'http://'+xhost+'/series/yasser/P51fBImqIu/'+id_+'.'+ext
			if ( season == saison):
				if xua!='':
					link  = strwithmeta(link,{'User-Agent' : xua})
				params = {'name':'categories','category' : 'video','url': link,'title':titre,'icon':img_,'desc':link,'hst':'direct'} 
				self.addVideo(params)	
						
 
	def xtreamvoditm(self,cItem):
		Url=cItem['url'] 
		ua=cItem['ua']
		Url=Url.replace('dmtn-tv.com/','dmtn-tv.com:8080/')
		sts, data = self.cm.getPage(Url)
		Live_Cat_data = re.findall('<title>(.*?)</title>.*?CDATA\[(.*?)].*?<description>(.*?)</description>.*?CDATA\[(.*?)]',data, re.S)
		for (titre,img,desc,Url) in Live_Cat_data:
			name = base64.b64decode(titre)
			if (desc != ''): desc = base64.b64decode(desc)
			#desc = re.sub('BACKDROP.*GENRE', 'GENRE', desc)
			desc_genre=''
			desc_date=''
			desc_rate=''
			desc_desc=''
			desc_tab = desc.splitlines()
			for desc1 in desc_tab:
				desc_data = re.findall('(.*?):(.*)',desc1, re.S)
				for (t_,c_) in desc_data:
					if (t_ == 'GENRE'):
						desc_genre=c_
					elif (t_ == 'RELEASEDATE'):
						desc_date=c_
					elif (t_ == 'RATING'):
						desc_rate=c_				
					elif (t_ == 'PLOT'):
						desc_desc=c_				
				#printDBG('fffffffffffffffffffffff'+desc1)
			
			descf= '\c0000??00'+'Genre:'+'\c00??????'+desc_genre+'\n'
			descf= descf+'\c0000??00'+'Rate:'+'\c00??????'+desc_rate+'/10\n'
			descf= descf+'\c0000??00'+'Release Date:'+'\c00??????'+desc_date+'\n'			
			desc_desc='\c0000??00'+'Plot:'+'\c00??????'+desc_desc
			
			descf= descf+desc_desc
			Url=Url.replace('dmtn-tv.com/','dmtn-tv.com:8080/')
			if ua!='':
				Url  = strwithmeta(Url,{'User-Agent' : ua})
			params = {'name':'categories','category' : 'video','url': Url,'title':name,'icon':img,'desc':descf,'hst':'direct'} 
			self.addVideo(params)

	def liveCategories(self):

		xuser = config.plugins.iptvplayer.ts_xtream_user.value
		xpass = config.plugins.iptvplayer.ts_xtream_pass.value	
		xhost = config.plugins.iptvplayer.ts_xtream_host.value	
		xua = config.plugins.iptvplayer.ts_xtream_ua.value
		if ((xuser!='') and (xpass!='') and (xhost!='')):
			url1='http://'+xhost+'/enigma2.php?username='+xuser+'&password='+xpass+'&type=get_live_categories'
			printDBG('eeee11111111110'+str(url1))			 
			sts, data = self.cm.getPage(url1)
			printDBG('eeee'+str(data)+str(sts))
			Live_Cat_data = re.findall('<title>(.*?)</title>.*?<description>(.*?)</description>.*?CDATA\[(.*?)]',str(data), re.S)
			for (titre,desc,Url) in Live_Cat_data:
				name = base64.b64decode(titre)
				name = re.sub('\|.*?\|', '', name)
				name = re.sub('\[.*', '', name).strip()			
				desc = base64.b64decode(desc)
				params = {'good_for_fav':True,'name':'categories','category' : 'livegroupid','url': Url,'title':name,'desc':desc,'ua':xua} 
				self.addDir(params)
		else:
			params = {'title':'Please configure xstream first','icon':'','desc':'Please configure xstream first, (add user,pass &host in tsiplayer params)'}
			self.addMarker(params)	
	
	def liveChanelsList(self,cItem):
		Url=cItem['url']
		ua=cItem['ua']
		printDBG('eeee1111111111'+str(Url))
		Url=Url.replace('dmtn-tv.com/','dmtn-tv.com:8080/')
		sts, data = self.cm.getPage(Url)
		data=data.replace('</title><description/>','</title><description></description>')
		Live_Cat_data = re.findall('<title>(.*?)</title>.*?<description>(.*?)</description>.*?CDATA\[(.*?)].*?CDATA\[(.*?)]',data, re.S)
		for (titre,desc,img,url) in Live_Cat_data:
			name = base64.b64decode(titre)
			name = re.sub('\|..\| ', '', name)
			name = re.sub('\[.*', '', name).strip()			
			if (desc != ''): desc = base64.b64decode(desc)
			if '---'in name:
				self.addMarker({'title':'\c0000??00'+name,'icon':img,'desc':desc})	
			elif '***'in name:
				self.addMarker({'title':'\c00????00'+name,'icon':img,'desc':desc})	
			else:
				url=url.replace('dmtn-tv.com/','dmtn-tv.com:8080/')
				if ua!='':
					url  = strwithmeta(url,{'User-Agent' : ua})
				params = {'name':'categories','category' : 'video','url': url,'title':name,'icon':img,'desc':desc,'hst':'direct'} 
				self.addVideo(params)		
		
		
		
###################################################
# HOST Yalla-Shoot
###################################################			
		
	def YallaVideos(self, cItem):
		pic0=cItem['icon']
		page =cItem['page']		
		url = 'http://www.yalla-shoot.com/live/video.php?page='+str(page)
		sts, data = self.cm.getPage(url)
		Liste_els = re.findall('<div class="col-md-3 col-sm-6 goals-item">.*?href="(.*?)".*?src="(.*?)".*?title="(.*?)">(.*?)<.*?</i>(.*?)<.*?</i>(.*?)<', data, re.S)
		for (URL,pic,desc,titre,date_,desc2) in Liste_els:
			params = {'good_for_fav':True,'name':'categories','category' : 'video','url':URL,'title': titre,'desc':desc,'icon':pic,'hst':'yallashoot'} 
			self.addVideo(params)
		params = {'title':'Page '+str(page+1),'page':page+1,'category' : 'yallashoot','icon':pic0} 
		self.addDir(params)		
		
		
		
		
###################################################
# HOST sporttv
###################################################	
	def sporttv(self,cItem): 		
		gnr=cItem['gnr']
	
		if gnr=='sporttv':  
			app_key = 'gzOBOoPZuS' 
		else:
			app_key = 'KMECeImAPk'		
		

		URL = ''
		ua = ''	
		sUrl = 'http://samdev.club/staging/public/api/free/getcategory'
		
		post_data = {'app_key':app_key}
		sts, data = self.cm.getPage(sUrl, post_data=post_data)
		t_data = re.findall('"webview":.*?url":"(.*?)".*?"user_agent":"(.*?)"', data, re.S)	
		if t_data:
			ua = t_data[0][1] 
			URL = t_data[0][0].replace('\/','/')	
		HTTP_HEADER= { 'User-Agent':'Mozilla/5.0 (Linux; Android 4.4.2; SAMSUNG-SM-N900A Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Safari/537.36','X-Requested-With':'com.sportstv20.app' }
		sts, data = self.cm.getPage(URL, {'header':HTTP_HEADER})
		URL_data = re.findall('href="(.*?)".*?src="(.*?)"', data, re.S)	
		for (url,image) in URL_data:
			if str(url).startswith('http://tvhistories.com') or (str(url).startswith('http://wssfree.com')):
				titre_data = re.findall('//.*/.*/(.*?).php', str(url), re.S)
				if titre_data:
					titre = str(titre_data[0])
					titre=titre.replace('wss','')
					params = {'good_for_fav':True,'name':'categories','category' : 'video','url': str(url),'title':titre,'icon':image,'desc':str(url),'hst':'sporttv','ua':ua} 
					self.addVideo(params)						

					
###################################################
# HOST Arablionz
###################################################	
	def Arablionz_Menu(self,cItem):		
		gnr=cItem['gnr']
		img_=cItem['icon'] 
		url='http://arblionz.com/'
		sts, data = self.cm.getPage(url)
		cat_film_data=re.findall('href="/cat/افلام">(.*?)</ul>', data, re.S) 
		cat_serie_data=re.findall('href="/cat/مسلسلات">(.*?)<a href="https://arblionz.com/category/برامج-تليفزيونية/">', data, re.S) 
		cat_music_data=re.findall('href="https://arblionz.com/category/اغاني-وكليبات/">(.*?)</ul>', data, re.S) 
	
		if gnr=='film':
			params = {'name':'categories','category' : 'arablionz_itm','url': 'https://arblionz.com/cat/%D8%A7%D9%81%D9%84%D8%A7%D9%85','title':'أفلام','desc':'','page':1,'icon':img_,'gnr':gnr} 
			self.addDir(params)		
			if cat_film_data:
				cat_el=re.findall('<li.*?href="(.*?)">(.*?)<', cat_film_data[0] , re.S)
				for (url,titre) in cat_el:
					params = {'name':'categories','category' : 'arablionz_itm','url': url,'title':titre,'desc':titre,'page':1,'icon':img_,'gnr':gnr} 
					self.addDir(params)	
		elif gnr=='serie':
			params = {'name':'categories','category' : 'arablionz_itm','url': 'https://arblionz.com/cat/%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA','title':'مسلسلات','desc':'','page':1,'icon':img_,'gnr':gnr} 
			self.addDir(params)		
			if cat_serie_data:
				cat_el=re.findall('<li.*?href="(.*?)">(.*?)<', cat_serie_data[0], re.S)
				for (url,titre) in cat_el:
					params = {'name':'categories','category' : 'arablionz_itm','url': url,'title':titre,'desc':titre,'page':1,'icon':img_,'gnr':gnr} 
					self.addDir(params)
		elif gnr=='music':
			params = {'name':'categories','category' : 'arablionz_itm','url': 'https://arblionz.com/category/%D8%A7%D8%BA%D8%A7%D9%86%D9%8A-%D9%88%D9%83%D9%84%D9%8A%D8%A8%D8%A7%D8%AA/','title':'اغاني وكليبات','desc':'','page':1,'icon':img_,'gnr':gnr} 
			self.addDir(params)
			if cat_music_data:
				cat_el=re.findall('<li.*?href="(.*?)">(.*?)<', cat_music_data[0] , re.S)
				for (url,titre) in cat_el:
					params = {'name':'categories','category' : 'arablionz_itm','url': url,'title':titre,'desc':titre,'page':1,'icon':img_,'gnr':gnr} 
					self.addDir(params)




					
 	def Arablionz_Items(self,cItem):		
		gnr=cItem['gnr']
		img_=cItem['icon'] 
		url=cItem['url']
		titre=cItem['title']
		if gnr=='items':
			sts, data = self.cm.getPage(url)
			desc=''
			desc_data=re.findall('<h2>.*?<p>(.*?)</p>', data, re.S)
			if desc_data:
				desc=desc_data[0]
				
			info_data=re.findall('info-item\">.*?<span>(.*?)</span>.*?\">(.*?)<', data, re.S)
			itm_=''
			for (itm_n,itm_d) in info_data:
				itm_=itm_+' | ' + itm_n+itm_d		
			desc=itm_+'\n'+'|'+'\n'+desc 
			 
			
			cat_data=re.findall('class="list-episodes">(.*?)</ul>', data, re.S)		
			params = {'good_for_fav':True,'name':'categories','category' : 'video','url': url,'title':titre,'desc':itm_,'icon':img_,'hst':'arablionz'} 
			self.addVideo(params)
			if cat_data:
				data1=cat_data[0]
				cat_data=re.findall('<li>.*?href="(.*?)".*?numEp">(.*?)<', data1, re.S)				
				for(url_,titre_) in cat_data:
					titre_=titre_.replace('\n',' ')	
					if '/season/' not in url_:
						params = {'good_for_fav':True,'name':'categories','category' : 'video','url': url_,'title':'E'+titre_,'desc':url_,'icon':img_,'hst':'arablionz'} 
						self.addVideo(params)	
		
		else: 
			page=cItem['page']
			sUrl=url+'?page='+str(page)
			sts, data = self.cm.getPage(sUrl)		 
			cat_data=re.findall('<div class="row">(.*?)class="pagination">', data, re.S)
			if cat_data:
				data=cat_data[0]
				cat_data=re.findall('src="(.*?)".*?href="(.*?)".*?">(.*?)<.*?<h3>(.*?)<', data, re.S)
				for (image,url1,desc,name_eng) in cat_data:
					params = {'good_for_fav':True,'name':'categories','category' : 'arablionz_itm','url': url1,'title':name_eng,'desc':desc,'icon':image,'gnr':'items'} 
					self.addDir(params)							
				params = {'good_for_fav':True,'name':'categories','category' : 'arablionz_itm','url': url,'title':'Page Suivante','page':page+1,'desc':'Page Suivante','icon':img_,'gnr':gnr} 
				self.addDir(params)	
		
	
###################################################
# HOST 33sk
###################################################	
	def _33sk_itm(self,cItem):		
		gnr=cItem['gnr']
		img_=cItem['icon'] 
		url=cItem['url'] 
		if gnr=='film' or gnr=='last':
			page=cItem['page'] 
			sUrl=url+'p'+str(page)+'.html'
			sts, data = self.cm.getPage(sUrl)			
			cat_data=re.findall('<div class="article">.*?href=\'(.*?)\'>(.*?)<.*?src=\'(.*?)\'', data, re.S)		
			for (url1,name_eng,image) in cat_data:
				image='https://33sk.tv/'+image
				name_eng=name_eng.replace('\n',' ')	
				params = {'good_for_fav':True,'name':'categories','category' : '33sk_itm','url': url1,'title':name_eng.strip(),'desc':'','icon':image,'gnr':'item'} 
				self.addDir(params)	
			params = {'good_for_fav':True,'name':'categories','category' : '33sk_itm','url': url,'title':'Page Suivante','page':page+1,'desc':'Page Suivante','icon':img_,'gnr':gnr} 
			self.addDir(params)					
				
				
		elif gnr=='serie':
			sUrl=url
			sts, data = self.cm.getPage(sUrl)		 	
			cat_data=re.findall('"itemIMG">.*?src="(.*?)".*?href="(.*?)">(.*?)<', data, re.S)		
			for (image,url1,name_eng) in cat_data:
				url1='https://33sk.tv/'+url1
				params = {'good_for_fav':True,'name':'categories','category' : '33sk_itm','url': url1,'title':name_eng.strip(),'desc':'','icon':image,'gnr':'item'} 
				self.addDir(params)	
			
			
		elif gnr=='item':
			titre=cItem['title'] 
			sUrl=str(url).strip().replace('&amp;','?')
			HTTP_HEADER = {'Host':'33sk.tv','Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8','Accept-Encoding':'gzip, deflate, br','Referer':'https://33sk.tv/vb/','User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0'}			
			printDBG('11111111111111111111111'+sUrl)  
			sts, data = self.cm.getPage(sUrl, {'header':HTTP_HEADER})	
			#resp = requests.get(sUrl)
			#data =resp.text
			#req = urllib2.Request(sUrl,headers)
			#response = urllib2.urlopen(req)
			#data = response.read()
			
			
			
			
			
			printDBG('00000000000'+data)			
			cat_data=re.findall('<div class="container-fluid">.*?href="(.*?)".*?">(.*?)<', data, re.S)	
			if cat_data:
				printDBG('2222222222222')
				for (url1,name_eng) in cat_data:
					url1='https://33sk.tv/'+url1
					params = {'good_for_fav':True,'name':'categories','category' : 'video','url': url1,'title':name_eng.strip(),'desc':'','icon':img_,'hst':'33sk'} 
					self.addVideo(params)				
			else:
				params = {'good_for_fav':True,'name':'categories','category' : 'video','url': sUrl,'title':titre,'desc':'','icon':img_,'hst':'33sk'} 
				self.addVideo(params)		
			
			
			
		
		
###################################################
# HOST Cimaclub
###################################################	
	def Cimaclub_Menu(self,cItem):		
		gnr=cItem['gnr']
		img_=cItem['icon'] 
		url='http://cimaclub.com/'
		sts, data = self.cm.getPage(url)		
		cat_data=re.findall('<ul class="sub-menu">(.*?)</ul>', data, re.S)
		if cat_data:
			data1=''
			if gnr=='film':
				data1=cat_data[0]
			elif gnr=='serie':
				data1=cat_data[1] 		
			elif gnr=='other':
				data1=cat_data[2]	
			cat_el=re.findall('<li.*?href="(.*?)">(.*?)<', data1, re.S)
			for (url,titre) in cat_el:
				params = {'name':'categories','category' : 'cimaclub_itm','url': url,'title':titre,'desc':titre,'page':1,'icon':img_,'gnr':gnr} 
				self.addDir(params)				

				
	def Cimaclub_Items(self,cItem):		
		gnr=cItem['gnr']
		img_=cItem['icon'] 
		url=cItem['url']
		titre_=cItem['title']
		if gnr=='items':
			lst=[]
			tab=[] 
			sts, data = self.cm.getPage(url)		
			cat_data=re.findall('<div data-season="(.*?)".*?href="(.*?)">(.*?)<', data, re.S)		
			if cat_data:
				cat_data2=re.findall('<div class="season" data-filter="(.*?)">(.*?)<', data, re.S)
				for (dat_,name_) in cat_data2:
					elem={'code':dat_.strip(),'name' : name_.strip(),'len':len(dat_.strip())}
					lst.append(elem)
				for (data,url,titre) in cat_data:
					data=data.strip()
					fnd=0
					for elm in lst:
						if str(elm['code'])==str(data):
							data=elm['name']
							fnd=1
							break
					if fnd==1:
						tab.append((data,int(titre.strip()),url))
				tab=sorted(tab, key = lambda x: (x[0], x[1]))		
				for(data_,titre_,url_) in tab:
					data_=data_.replace('موسم ','Saison ') 
					Ep=str(titre_)
					if len(Ep)==1: 
						Ep='0'+Ep
					Ep=data_+' - E'+Ep
					params = {'good_for_fav':True,'name':'categories','category' : 'video','url': url_,'title':Ep,'desc':'','icon':img_,'hst':'cimaclub'} 
					self.addVideo(params)					
			else:
				if 'اجزاء السلسلة' in data: 
					cat_data=re.findall('<div class="moviesBlocks">(.*?)<div class="moviesBlocks">', data, re.S)
					if cat_data:
						data2=cat_data[0]
						cat_data=re.findall('<div class="movie">.*?href="(.*?)".*?src="(.*?)".*?<h2>(.*?)<.*?<p>(.*?)</p>', data2, re.S)
						for (url,image,name_eng,desc) in cat_data:
							params = {'good_for_fav':True,'name':'categories','category' : 'video','url': url,'title':name_eng,'desc':desc,'icon':image,'hst':'cimaclub'} 
							self.addVideo(params)	
				else:
					params = {'good_for_fav':True,'name':'categories','category' : 'video','url': url,'title':titre_,'desc':'','icon':img_,'hst':'cimaclub'} 
					self.addVideo(params)	
		
	
		else:
			page=cItem['page']
			sUrl=url+'page/'+str(page)+'/' 
			sts, data = self.cm.getPage(sUrl)		
			cat_data=re.findall('<div class="movie">.*?href="(.*?)".*?src="(.*?)".*?<h2>(.*?)<.*?<p>(.*?)</p>', data, re.S)
			for (url1,image,name_eng,desc) in cat_data:
				name_eng=name_eng.replace(' اون لاين','')
				params = {'good_for_fav':True,'name':'categories','category' : 'cimaclub_itm','url': url1,'title':name_eng,'desc':desc,'icon':image,'gnr':'items'} 
				self.addDir(params)							
			params = {'good_for_fav':True,'name':'categories','category' : 'cimaclub_itm','url': url,'title':'Page Suivante','page':page+1,'desc':'Page Suivante','icon':img_,'gnr':gnr} 
			self.addDir(params)	
				
				


			
###################################################
# HOST Animkom.com
###################################################	
		
	
		
	def listtypes(self,cItem):
		gnr=cItem['gnr']
		img_=cItem['icon']
		Url='http://www.animekom.com/'
		Spatern1='<ul class="cat-list"(.*?)</ul>'
		Spatern2='<li><a href="(.*?)".*?</i>(.*?)<'
		sts, data = self.cm.getPage(Url)
		Cat_data = re.findall(Spatern1,data, re.S)
		if Cat_data:
			if gnr=='Anim':
				list_cat=re.findall(Spatern2, Cat_data[0], re.S)
			else:
				list_cat=re.findall(Spatern2, Cat_data[1], re.S)
			for (url,titre) in list_cat:
				if '18' not in titre:
					params = {'good_for_fav':True,'name':'categories','category' : 'lstitm','Url': url,'title':titre,'desc':titre,'page':1,'icon':img_,'gnr':'anim'} 
					self.addDir(params)
					
	def listitems(self,cItem):
		Url=cItem['Url']
		page=cItem['page']
		img_=cItem['icon']	
		sUrl=Url+'page/'+str(page)+'/'
		sts, data = self.cm.getPage(sUrl) 
		Liste_films_data = re.findall('div id="content">(.*?)<style>#dle-content{width:650px;}</style>', data, re.S)
		if Liste_films_data:
			Liste_films = re.findall('alt="(.*?)".*?img src="(.*?)".*?href="(.*?)">(.*?)&.*?desc">(.*?)<', Liste_films_data[0], re.S)
			for (name_eng,image,url,name_ar,desc) in Liste_films:
				if image.startswith('/'):
					image='https://www.animekom.com'+image
				params = {'good_for_fav':True,'name':'categories','category' : 'video','Url': url,'title':name_ar+' ('+name_eng+')','desc':desc,'icon':image,'hst':'animkom'} 
				self.addVideo(params)	
		params = {'good_for_fav':True,'name':'categories','category' : 'lstitm','Url': Url,'title':'Page Suivante','page':page+1,'desc':'Page Suivante','icon':img_} 
		self.addDir(params)


###################################################
# HOST Movs4u.TV
###################################################						
									
	def movs_lst_type(self,cItem):	
		gnr=cItem['gnr']
		img_=cItem['icon']		
		sUrl='https://www.movs4u.tv/'
		sts, data = self.cm.getPage(sUrl) 
		Liste_films_data = re.findall('class="sub-menu">(.*?)\/ul', data, re.S)
		if Liste_films_data :
			if gnr=='Film_qual':
				films_data=Liste_films_data[1]
			else:
				films_data=Liste_films_data[0]		
			Liste_films_data = re.findall('<a href="(.*?)">(.*?)<', films_data, re.S)	
			for (url,titre) in Liste_films_data:
				params = {'good_for_fav':True,'name':'categories','category' : 'list_items_movs4u','Url': url,'title':titre,'desc':titre,'page':1,'icon':img_,'gnr':gnr} 
				self.addDir(params)	
				
	def list_items_movs4u(self,cItem):
		page=cItem['page']
		url_=cItem['Url']+'page/'+str(page)+'/'
		printDBG('list_items_movs4u='+url_)	
		gnr=cItem['gnr']		
		img_=cItem['icon']
		sts, data = self.cm.getPage(url_) 
		Liste_films_data = re.findall('<h1(.*?)(pagination">|<div class="copy">)', data, re.S)
		if Liste_films_data :
			films_data=Liste_films_data[0][0]	
			if gnr=='film':
				Liste_films_data = re.findall('<article id=.*?src="(.*?)".*?alt="(.*?)".*?quality">(.*?)<.*?href="(.*?)".*?metadata">(.*?)</div>.*?texto">(.*?)<', films_data, re.S)	
				for (image,name_eng,qual,url,meta,desc) in Liste_films_data:
					meta_=''
					meta_data = re.findall('<span.*?>(.*?)<', meta, re.S)	
					for (mt) in meta_data:
						if meta_=='':
							meta_= mt 
						else:
							meta_= meta_ + ' | ' + mt
					desc = meta_ +'\n'+ desc
					params = {'good_for_fav':True,'name':'categories','category' : 'video','Url': url,'title':name_eng +'\c0000???? ('+qual+')','desc':desc,'icon':image,'hst':'movs4u'} 
					self.addVideo(params)	
			elif gnr=='Film_qual':
				Liste_films_data = re.findall('class="item".*?href="(.*?)".*?src="(.*?)".*?alt="(.*?)".*?type">(.*?)<', films_data, re.S)	
				for (url,image,name_eng,nat) in Liste_films_data:
					params = {'good_for_fav':True,'name':'categories','category' : 'video','Url': url,'title':name_eng +'\c0000???? ('+nat+')','desc':'','icon':image,'hst':'movs4u'} 
					self.addVideo(params)					
			elif gnr=='serie':
				Liste_films_data = re.findall('article id.*?src="(.*?)".*?alt="(.*?)".*?href="(.*?)".*?"texto">(.*?)<', films_data, re.S)	
				for (image,name_eng,url,desc) in Liste_films_data:
					params = {'good_for_fav':True,'name':'categories','category' : 'list_items_movs4u','Url': url,'title':name_eng,'desc':desc,'icon':image,'gnr':'serie_ep','page':1} 
					self.addDir(params)			 
			elif gnr=='Film_coll':
				Liste_films_data = re.findall('class="item.*?href="(.*?)".*?src="(.*?)".*?alt="(.*?)"', films_data, re.S)	
				for (url,image,name_eng) in Liste_films_data:
					params = {'good_for_fav':True,'name':'categories','category' : 'list_items_movs4u','Url': url,'title':name_eng,'desc':'','icon':image,'hst':'movs4u','gnr':'one_film_col','page':1} 
					self.addDir(params)					
			elif gnr=='one_film_col':
				Liste_films_data = re.findall('class="item.*?href="(.*?)".*?src="(.*?)".*?alt="(.*?)".*?quality">(.*?)<', films_data, re.S)	
				for (url,image,name_eng,qual) in Liste_films_data:
					params = {'good_for_fav':True,'name':'categories','category' : 'video','Url': url,'title':name_eng +'\c0000???? ('+qual+')','desc':'','icon':image,'hst':'movs4u'} 
					self.addVideo(params)
			elif gnr=='serie_ep':
				Liste_films_data1 = re.findall('<h2>Video trailer.*?src="(.*?)"', data, re.S)
				if Liste_films_data1:			
					params = {'name':'categories','category' : 'video','Url': Liste_films_data1[0],'title':'Trailer','desc':'','icon':img_,'hst':'none'} 
					self.addVideo(params)			
				Liste_films_data = re.findall('<div id=\'seasons\'>(.*?)</script>', data, re.S)
				if Liste_films_data:
					films_data=Liste_films_data[0]
					Liste_films_data = re.findall("<span class='title'>(.*?)<(.*?)<\/ul>", films_data, re.S)
					for (season,data_s) in Liste_films_data:
						params = {'name':'categories','category' : 'video','Url': '','title':'\c00????00'+season.replace('الموسم',''),'desc':'','icon':img_,'hst':'movs4u'} 
						self.addDir(params)
						films_data = re.findall("<li>.*?src='(.*?)'.*?numerando'>(.*?)<.*?href='(.*?)'>(.*?)<", data_s, re.S)
						for (image,name_eng,url,name2) in films_data:
							params = {'good_for_fav':True,'name':'categories','category' : 'video','Url': url,'title':name_eng +'\c0000????  # '+name2,'desc':'','icon':image,'hst':'movs4u'} 
							self.addVideo(params)
			if gnr!='serie_ep':
				params = {'category':'list_items_movs4u','title': 'Next Page','Url':cItem['Url'],'gnr':gnr,'page':page+1,'icon':img_}
				self.addDir(params)							
###################################################
# http://cima4u.tv/
###################################################		


	def Cima4u_itemsS(self, cItem):		
		url1=cItem['link']
		page=cItem['page']
		sts, data = self.cm.getPage(url1+'?PageID='+str(page))
		Liste_els = re.findall('</h2>(.*?)(class="pagination"|<footer>)', data, re.S)
		
		if Liste_els:
			films_list = re.findall('class="block">.*?<a href="(.*?)".*?background-image:url\((.*?)\).*?"boxtitle">(.*?)<', Liste_els[0][0], re.S)		
			for (url,image,titre) in films_list:
				params = {'good_for_fav':True,'name':'categories','category' : 'Cima4uS','url': url,'title':titre,'desc':url,'icon':image} 
				self.addDir(params)
		params = {'title':'Page '+str(page+1),'page':page+1,'category' : 'Cima4uSe','link':url1,'icon':image} 
		self.addDir(params)				
	
	def Cima4u_items(self, cItem):		
		url1=cItem['link']
		page=cItem['page']
		sts, data = self.cm.getPage(url1+'page/'+str(page)+'/')
		Liste_els = re.findall('</h2>(.*?)(class="pagination"|<footer>)', data, re.S)
		
		if Liste_els:
			films_list = re.findall('class="block">.*?<a href="(.*?)".*?background-image:url\((.*?)\).*?"boxtitle">(.*?)<.*?"boxdetil">(.*?)<\/div>', Liste_els[0][0], re.S)		
			for (url,image,titre,desc) in films_list:
				titre=titre.replace('مشاهدة فيلم ','')
				params = {'good_for_fav':True,'name':'categories','category' : 'video','Url': url,'title':titre,'desc':desc,'icon':image,'hst':'cima4u'} 
				self.addVideo(params)	
		params = {'title':'Page '+str(page+1),'page':page+1,'category' : 'Cima4uFS','link':url1,'icon':image} 
		self.addDir(params)									
					
	def Cima4u_Episodes(self, cItem):
		URL=cItem['url'] 
		icon=cItem['icon'] 
		titre=cItem['title'] 	
		sts, data = self.cm.getPage(URL)
		Liste_els = re.findall('حلقات المسلسل(.*?)<footer>', data, re.S)
		if Liste_els:	
			Liste_els_2 =  re.findall('<a href="(.*?)".*?</span>(.*?)<', Liste_els[0], re.S)
			for (url,titre) in Liste_els_2:
				titre=titre.replace('(','').replace(')','')
				params = {'good_for_fav':True,'name':'categories','category' : 'video','Url': url,'title':titre,'desc':url,'icon':icon,'hst':'cima4u'} 
				self.addVideo(params)		


		
###################################################
# Main
###################################################	
						
	def handleService(self, index, refresh = 0, searchPattern = '', searchType = ''):
		printDBG('handleService start')
		CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)
		name     = self.currItem.get("name", '')
		category = self.currItem.get("category", '')
		printDBG( "handleService: || name[%s], category[%s] " % (name, category) )
		self.currList = []
		self.cacheLinks = {}
		#MAIN MENU
		if name == None:
			self.MainCat()
			
		# Update	
		elif category == 'update':
			self.Update()
		elif category == 'update_now':
			self.update_now(self.currItem)			
		elif category == 'log':
			self.GetCommits()
		elif category == 'thx':
			self.thx()		
		#CATEGORIES
		elif category == 'FilmsSeries':
			self.listsTab(self.FilmsSeries_TAB, {'name':'FilmsSeries'})
		elif category == 'Live':
			self.listsTab(self.Live_TAB, {'name':'Live'})
		elif category == 'Sports':
			self.listsTab(self.Sports_TAB, {'name':'Sports'})
		#Animekom	
		elif category == 'animekom':
			self.listsTab(self.Animekom_TAB, {'name':'animekom'})				
		elif category == 'lstcata':
			self.listsTab(self.Anim_CAT_TAB, {'name':'Anims'})	
		elif category == 'lstcatf':
			self.listsTab(self.Film_CAT_TAB, {'name':'Films'})
		elif category == 'lstitm':
			self.listitems(self.currItem)
		elif category == 'lsttyp':
			self.listtypes(self.currItem)
		#Movs4u	
		elif category == 'movs4u':
			self.listsTab(self.Movs4u_TAB, {'name':'movs4u'})				
		elif category == 'cat_films_movs4u':
			self.listsTab(self.Movs4u_Film_CAT_TAB, {'name':'movs4u'})
		elif category == 'movs4u_lsttyp':
			self.movs_lst_type(self.currItem)
		elif category == 'list_items_movs4u':
			self.list_items_movs4u(self.currItem)			
		#Cima4u
		elif category == 'cima4u':
			self.listsTab(self.Cima4u_TAB, {'name':'Cima4u'})			
		elif category == 'Cima4u_films_t':
			self.listsTab(self.Cima4u_films_Cat, {'name':'Cima4u'})
		elif category == 'Cima4u_series_t':
			self.listsTab(self.Cima4u_series_Cat, {'name':'Cima4u'})
		elif category == 'Cima4uFS':
			self.Cima4u_items(self.currItem)
		elif category == 'Cima4uSe':
			self.Cima4u_itemsS(self.currItem)
		elif category == 'Cima4uS':
			self.Cima4u_Episodes(self.currItem)
		#cimaclub.com/
		elif category == 'cimaclub':
			self.listsTab(self.Cimaclub_TAB, {'name':'Cimaclub'})			
		elif category == 'Cimaclub_cat':
			self.Cimaclub_Menu(self.currItem)
		elif category == 'cimaclub_itm':
			self.Cimaclub_Items(self.currItem)
		#33sk
		elif category == '33sk':
			self.listsTab(self._33sk_TAB, {'name':'33sk'})	
		elif category == '33sk_itm':
			self._33sk_itm(self.currItem)	

		#arablionz
		elif category == 'arablionz':
			self.listsTab(self.Arablionz_TAB, {'name':'arablionz'})			
		elif category == 'Arablionz_cat':
			self.Arablionz_Menu(self.currItem)
		elif category == 'arablionz_itm':
			self.Arablionz_Items(self.currItem)
			
		#sporttv
		elif category == 'sporttv':
			self.sporttv(self.currItem)	
		elif category == 'livegroupid':
			self.liveChanelsList(self.currItem)			
		elif category == 'yallashoot':
			self.YallaVideos(self.currItem)	
		#xstream
		elif category == 'xtream_live':
			self.liveCategories()				
		elif category == 'livegroupid':
			self.liveChanelsList(self.currItem)
		elif category == 'xtream_vod':
			self.xtreamvodcat()				
		elif category == 'xtream_itm_vod':
			self.xtreamvoditm(self.currItem)		
		elif category == 'vodserieid':
			self.VodSerieSaisons(self.currItem)			
		elif category == 'vodsaisonid':
			self.VodSerieEpisodes(self.currItem)	

		#Search
		elif category == 'search':
			self.listSearchResult(self.currItem,searchPattern, searchType)	
		elif category == '_next_page':
			self.listSearchResult(self.currItem,'', '')			
		elif category == "search_history":
			self.listsHistory({'name':'history', 'category': 'search'}, 'desc', "Type: ")
			
		else:
			printExc()

		CBaseHostClass.endHandleService(self, index, refresh)
		
	def listSearchResult(self, cItem, searchPattern, searchType):
		hst=cItem['hst']
		cat=cItem['category']
		page=cItem['page']
		if cat=='_next_page':
			str_ch = cItem['searchPattern']
		else:
			str_ch = searchPattern
		if hst=='animkom':
			url_='https://www.animekom.com/index.php?do=search'
			page=cItem['page']
			post_data = {'do':'search','subaction':'search','search_start':page,'full_search':0,'result_from':((page-1)*20)+1,'story':str_ch}
			sts, data = self.cm.getPage(url_, post_data=post_data)
			Liste_films_data = re.findall('<div class="search-result">.*?href="(.*?)".*?src="(.*?)".*?alt="(.*?)".*?title">.*?">(.*?)<.*?desc">(.*?)<', data, re.S)
			for (url,image,name_eng,name_ar,desc) in Liste_films_data:
				if image.startswith('/'):
					image='https://www.animekom.com'+image
				params = {'good_for_fav':True,'name':'categories','category' : 'video','Url': url,'title':name_ar,'desc':desc,'icon':image,'hst':'animkom'} 
				self.addVideo(params)		
		elif hst=='movs4u':
			url_='https://www.movs4u.tv/page/'+str(page)+'/?s='+str_ch
			sts, data = self.cm.getPage(url_)
			Liste_films_data = re.findall('"result-item">.*?href="(.*?)".*?src="(.*?)".*?alt="(.*?)".*?">(.*?)<', data, re.S)
			for (url,image,name_eng,type_) in Liste_films_data:
				if type_=='Movie':
					params = {'good_for_fav':True,'name':'categories','category' : 'video','Url': url,'title':name_eng,'desc':'','icon':image,'hst':'movs4u'} 
					self.addVideo(params)
				else:
					params = {'good_for_fav':True,'name':'categories','category' : 'list_items_movs4u','Url': url,'title':name_eng,'desc':'','icon':image,'gnr':'serie_ep','page':1} 
					self.addDir(params)					
		elif hst=='cimaclub':
			url_='http://cimaclub.com/page/'+str(page)+'/?s='+str_ch
			sts, data = self.cm.getPage(url_)
			cat_data=re.findall('<div class="movie">.*?href="(.*?)".*?src="(.*?)".*?<h2>(.*?)<.*?<p>(.*?)</p>', data, re.S)
			for (url1,image,name_eng,desc) in cat_data:
				params = {'good_for_fav':True,'name':'categories','category' : 'cimaclub_itm','url': url1,'title':name_eng,'desc':desc,'icon':image,'gnr':'items'} 
				self.addDir(params)
		
		elif hst=='cima4u':
			url_='http://cima4u.tv/page/'+str(page)+'/?s='+str_ch
			sts, data = self.cm.getPage(url_)
			films_list = re.findall('class="block">.*?<a href="(.*?)".*?background-image:url\((.*?)\).*?"boxtitle">(.*?)<.*?"boxdetil">(.*?)<\/div>', data, re.S)		
			for (url,image,titre,desc) in films_list:
				titre=titre.replace('مشاهدة فيلم ','')
				titre=titre.replace('مشاهدة مسلسل ','')
				params = {'good_for_fav':True,'name':'categories','category' : 'video','Url': url,'title':titre,'desc':desc,'icon':image,'hst':'cima4u'} 
				self.addVideo(params)
		elif hst=='arablionz':
			url_='http://arblionz.com/page/'+str(page)+'/?s='+str_ch
			sts, data = self.cm.getPage(url_)		
			cat_data=re.findall('<div class="lionSection">(.*?)<div class="pagination">', data, re.S)
			if cat_data:
				data=cat_data[0]
				cat_data=re.findall('<a href="(.*?)".*?image:url\((.*?)\).*?TitlePopover">(.*?)<.*?contentShow">(.*?)<', data, re.S)
				for (url1,image,name_eng,desc) in cat_data:
					params = {'good_for_fav':True,'name':'categories','category' : 'arablionz_itm','url': url1,'title':name_eng,'desc':desc,'icon':image,'gnr':'items'} 
					self.addDir(params)							


			
		params={'category':'_next_page','title': 'Page Suivante', 'search_item':False,'page':page+1,'searchPattern':str_ch,'hst':hst}
		self.addDir(params)	
		
	def getVideoLinks(self, videoUrl):
		printDBG("Movs4uCOM.getVideoLinks [%s]" % videoUrl)
		urlTab = []
		if videoUrl.startswith('hst'):
			_data = re.findall('hst#(.*?)#(.*?)#', videoUrl+'#', re.S)	
			hst=_data[0][0]
			videoUrl=_data[0][1]
		else:
			hst='none'
		if hst=='movs4u':
			if videoUrl.startswith('http'):
				sts, data = self.cm.getPage(videoUrl)
				_data2 = re.findall('<iframe.*?src="(.*?)"',data, re.S)		 		
				if _data2:
					videoUrl = _data2[0]
					if 'gdriveplayer' in videoUrl:
						_data3 = re.findall('link=(.*?)&',videoUrl, re.S)
						if _data3: 
							videoUrl = _data3[0]
					urlTab = self.up.getVideoLinkExt(videoUrl)
			else:
				post_data = {'action':'doo_player_ajax','post':videoUrl,'nume':'trailer','type':'movie'}
				sts, data2 = self.cm.getPage('https://www.movs4u.tv/wp-admin/admin-ajax.php', post_data=post_data)
				_data0 = re.findall('<iframe.*?src="(.*?)"',data2, re.S)
				if _data0:
					urlTab = self.up.getVideoLinkExt(_data0[0])
		elif hst=='cima4u':
			sUrl = 'http://live.cima4u.tv/structure/server.php?id='+videoUrl
			post_data = {'id':videoUrl}
			sts, data = self.cm.getPage(sUrl, post_data=post_data)
			Liste_els_3 = re.findall('src="(.*?)"', data, re.S)	
			if Liste_els_3:	
				urlTab = self.up.getVideoLinkExt(Liste_els_3[0])
		elif hst=='cimaclub':
			code,id=videoUrl.split('|')
			URL='http://cimaclub.com/wp-content/themes/Cimaclub/servers/server.php?q='+code+'&i='+id
			sts, data = self.cm.getPage(URL)
			data=data.lower() 
			_data2 = re.findall('<iframe.*?src=(.*?) ',data, re.S)
			if _data2:
				URL_=_data2[0]
				URL_=URL_.replace('"','')
				URL_=URL_.replace("'",'')
				if URL_.startswith('//'):
					URL_='http:'+URL_
				urlTab = self.up.getVideoLinkExt(URL_)
		elif hst=='arablionz':
			HTTP_HEADER= { 'X-Requested-With':'XMLHttpRequest' }

			printDBG('rrrrrrrrr'+videoUrl)
			sts, data = self.cm.getPage(videoUrl,{'header':HTTP_HEADER})
			printDBG('rrrrrrrrr0'+data)
			data=data.lower()
			_data2 = re.findall('<iframe.*?src=(.*?) ',data, re.S) 
			if _data2:
				URL_=_data2[0]
				URL_=URL_.replace('"','')
				URL_=URL_.replace("'",'')
				if URL_.startswith('//'):
					URL_='http:'+URL_ 
				urlTab = self.up.getVideoLinkExt(URL_)		 
			else:
				data=data.strip()
				if data.startswith('http'):
					urlTab = self.up.getVideoLinkExt(data)					
		
		
		else:
			urlTab = self.up.getVideoLinkExt(videoUrl)
		return urlTab
		
	def getLinksForVideo(self, cItem):
		printDBG("TVProart.getLinksForVideo [%s]" % cItem)

		name=cItem['title']
		hst=cItem['hst']
		urlTab = []
	
		if hst=='animkom':
			URL=cItem['Url']
			sts, sHtmlContent = self.cm.getPage(URL)
			Liste_Episodes_data = re.findall('<div id="accordian">(.*?)</div>', sHtmlContent, re.S)
			if Liste_Episodes_data:
				for (Lst_Episodes_data) in Liste_Episodes_data:	
					Name_Episodes_data = re.findall('</i>(.*?)<', Lst_Episodes_data, re.S)
					Name_Episode=Name_Episodes_data[0]
					lst_hoste_data = re.findall('><a data-link="(ht.*?)".?>(.*?)<', Lst_Episodes_data, re.S)				
					for (url,host) in lst_hoste_data:
						hostUrl=url.replace("www.", "")				
						raw1 =  re.findall('//(.*?)/', hostUrl, re.S)				
						hostUrl=raw1[0]
						host = host.replace(' ','')
						host = host.replace('\n','')
						host = host.replace('\\n','')
						Name_Episode = re.sub(' +',' ',Name_Episode)
						Name_Episode = Name_Episode.replace('\n','')
						Name_Episode = Name_Episode.replace('\\n','')
						if 'الحلقة' in Name_Episode:
							Name_Episode = 'Episode: '+Name_Episode.replace('الحلقة','')
						if 'الأوفا' in Name_Episode:
							Name_Episode = 'OVA: '+Name_Episode.replace('الأوفا','')
						urlTab.append({'name':Name_Episode+' \c0000????'+host, 'url':url, 'need_resolve':1,'hst':'animkom'})					
			else: 
				Liste_Episodes_data = re.findall('role="tablist">(.*?)</div>', sHtmlContent, re.S)
				if Liste_Episodes_data:
					lst_hoste_data = re.findall('<a data-link="(.*?)".*?"tab">(.*?)<', Liste_Episodes_data[0], re.S)				
					for (url,host) in lst_hoste_data:
						hostUrl=url.replace("www.", "")				
						raw1 =  re.findall('//(.*?)/', hostUrl, re.S)				
						hostUrl=raw1[0]
						urlTab.append({'name':host, 'url':url, 'need_resolve':1,'hst':'animkom'})	
		elif hst=='movs4u':
			URL=cItem['Url']
			sts, sHtmlContent = self.cm.getPage(URL)
			_data0 = re.findall("'trailer'>.*?'title'>(.*?)<.*?server'>(.*?)<.*?data-post='(.*?)'",sHtmlContent, re.S)
			if _data0:
				urlTab.append({'name':_data0[0][0].replace('- تريلر الفلم',':')+' ' +'\c0000????('+_data0[0][1]+')', 'url':'hst#movs4u#'+_data0[0][2], 'need_resolve':1})					
			_data = re.findall("data-url='(.*?)'.*?title'>(.*?)<.*?server'>(.*?)<",sHtmlContent, re.S)
			for (data_url,titre1,srv) in _data:
				titre1=titre1.replace('سيرفر مشاهدة رقم','Server:') 
				urlTab.append({'name':titre1+' ' +'\c0000????('+srv+')', 'url':'hst#movs4u#'+data_url, 'need_resolve':1})		
			return urlTab
		elif hst=='cima4u': 
			URL=cItem['Url'] 
			sts, data = self.cm.getPage(URL)
			Liste_els = re.findall('"embedURL" content="(.*?)"', data, re.S)		
			if Liste_els:
				URL = Liste_els[0]
			else:
				URL = URL
			sts, data = self.cm.getPage(URL)
			Liste_els = re.findall('"serversList">.*?<h2>(.*?)(<h2>|<footer>)', data, re.S)
			if Liste_els:		
				Liste_els_2 =  re.findall('data-link="(.*?)".*?/>(.*?)<', Liste_els[0][0], re.S)
				for (code,host_) in Liste_els_2:
					host_ = host_.replace(' ','')
					urlTab.append({'name':host_, 'url':'hst#cima4u#'+code, 'need_resolve':1})						
		elif hst=='cimaclub':
			URL=cItem['url']+'?view=1'		
			sts, data = self.cm.getPage(URL)
			code_data = re.findall("data: 'q=(.*?)&", data, re.S)
			if code_data:
				code=code_data[0]
				server_data = re.findall('data-server="(.*?)">(.*?)<', data, re.S)	
				for (id,name) in server_data:
					urlTab.append({'name':name, 'url':'hst#cimaclub#'+code+'|'+id, 'need_resolve':1})	
		elif hst=='arablionz':
			URL=cItem['url'].replace('/episode/','/watch/')
			URL=URL.replace('/film/','/watch/')
			URL=URL.replace('/post/','/watch/')
			sts, data = self.cm.getPage(URL)
			printDBG('ffffffurl='+URL)
			printDBG('ffffffdata='+data)
			server_data = re.findall('data-embedd="&lt.*?(SRC|src)=&quot;(.*?)&quot;.*?">(.*?)"', data, re.S)
			if server_data:
				for (x1,url,titre_) in server_data:
					hostUrl=url.replace("www.", "")				
					raw1 =  re.findall('//(.*?)/', hostUrl, re.S)				
					hostUrl=raw1[0]
					urlTab.append({'name':hostUrl, 'url':url, 'need_resolve':1})
			else:
				code_data = re.findall('data-embedd="(.*?)".*?alt="(.*?)"', data, re.S)
				id_data = re.findall("attr\('data-embedd'\).*?url: \"(.*?)\"", data, re.S)
				if id_data:
					for (code_,titre_) in code_data:
						url=id_data[0]+'&serverid='+code_
						urlTab.append({'name':titre_, 'url':'hst#arablionz#'+url, 'need_resolve':1})	

		elif hst=='sporttv':
			URL=cItem['url']
			ua=cItem['ua']			
			sts, data = self.cm.getPage(URL)
			data = re.sub('<!--.*?-->', '', data)
			URL_data = re.findall('<a.*?(http.*?)">(.*?)</', data, re.S)	
			for (url,titre) in URL_data:
				if (url.find('sportstv.club')<0)and (url.find('wssfree.com')<0)and(url.find('histats')<0)and (str(titre)!='.')and (str(titre)!='a'):
					URl = strwithmeta(url,{'User-Agent' : ua})
					titre = re.sub('<.*?>', '', str(titre))
					urlTab.append({'name':titre, 'url':URl , 'need_resolve':0})	
		
		elif hst=='yallashoot':
			url='http://www.yalla-shoot.com/live/'+cItem['url']	 
			URL_=''
			sts, data = self.cm.getPage(url)
			Liste_els = re.findall('<iframe.*?src="(.*?)"', data, re.S)
			if Liste_els:
				URL2=Liste_els[0]
				if 'ok.php?id=' in	URL2:
					Link = re.findall('php\?id=(.*?)(\?| |<)', data, re.S)
					if Link:
						URL_='http://ok.ru/videoembed/'+Link[0][0]
				elif 'youtube.php?ytid=' in	URL2:
					Link = re.findall('php\?ytid=(.*?)(\?| |<)', data, re.S)
					if Link:
						URL_='https://www.youtube.com/embed/'+Link[0][0]
				else:
					URL_=URL2
			if URL_.startswith('//'):
				URL_='http:'+URL_
			if URL_!='':
				name=URL_
				Link = re.findall(':(.*?)/', URL_, re.S)
				if Link:
					name=Link[0]				
				urlTab.append({'name':name, 'url':URL_, 'need_resolve':1,'hst':'none'})	
		
		elif hst=='direct':	
			urlTab.append({'name':name, 'url':cItem['url'], 'need_resolve':0,'hst':'none'})		
		else:
			urlTab.append({'name':name, 'url':cItem['url'], 'need_resolve':1,'hst':'none'})	
		return urlTab

class IPTVHost(CHostBase): 

	def __init__(self):
		CHostBase.__init__(self, TVProart(), True, [CDisplayListItem.TYPE_VIDEO, CDisplayListItem.TYPE_AUDIO])
